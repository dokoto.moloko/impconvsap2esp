#include "main.hpp"

int main(const int argc, const char* argv[])
{	
	#ifdef _MSC_VER
		std::locale::global(std::locale("spanish")); 
		std::cout.imbue(std::locale("spanish"));	
	#endif
	return cc::MainUnit::Run(argc, argv);
}


bool cc::MainUnit::Run(CoInt_t argc, CoCStr_t argv[]) 
{ 
	if (cc::args::Check(argc, argv) == false) return false;	
	
	cc::ConvSap2Esp conv(cc::args::m_args[cc::args::mc_parm_in].c_str(), cc::args::m_args[cc::args::mc_parm_out].c_str(), cc::args::m_args[cc::args::mc_parm_modo].c_str() );		
	if (conv.Conv() == false) return false;
	if (conv.Save() == false) return false;
	
	std::cout << "Fichero generado correctamente en : " << cc::args::m_args[cc::args::mc_parm_out] << std::endl;
	#ifdef _MSC_VER
		//std::cin.get();
	#endif

	return true;
}
